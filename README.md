EpicMusic
=========
Projet pour faire de la musique avec les buzzers des pcs, ne fonctionne que en 106 !

Lancer un musique
-----------------
### Pour les fichiers à la racine
Simplement faire `./nom_de_la_musique` (en étant dans le dossier)

### Pour game of thrones
Pour lancer plusieurs pistes synchronisées
```xml
cd got
./start <minute_de_lancement> <piste>
```

Créer sa musique
----------------

```xml
tempo=<tempo>
source ./rythme

<rythme> <note>
...
```

une note s'écrit de la facon suivante : `<rythme> <note>`
exemple:
```bash
c mi3 #mi3 chroche
b do2 #do2 blanche
silence #silence
```

les rythmes existants sont:
 - d : double chroche
 - c : croche
 - n : noir
 - np : noir pointé
 - b : blanche
 - r : ronde
 - silence : :)
 - demsilence : demi-silence
 
 Pour les notes, le `do3` represente le do entre la clé de fa et de sol
 
 ## Configuration
 
 * Pour éditer la façon dont est actionné le buffer, il faut modifier le fichier `note/note`
 * Tous les rythmes sont dans le fichiers rythme (une fonction par rythme)
 * Les notes dans le dossier note
 